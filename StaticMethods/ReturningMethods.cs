﻿namespace StaticMethods
{
    public static class ReturningMethods
    {
        // TODO #1-1: Add the static method here with name "ReturnInt" that should return -1234567 literal (int return type).

        // TODO #1-2: Add the static method here with name "ReturnUnsignedInt" that should return 1234567u literal (uint return type).

        // TODO #1-3: Add the static method here with name "ReturnLong" that should return -987654321L literal (long return type).

        // TODO #1-4: Add the static method here with name "ReturnUnsignedLong" that should return 987654321uL literal (ulong return type).

        // TODO #1-5: Add the static method here with name "ReturnFloat" that should return 1234.567f literal (float return type).

        // TODO #1-6: Add the static method here with name "ReturnDouble" that should return -9876.54321 literal (double return type).

        // TODO #1-7: Add the static method here with name "ReturnDecimal" that should return -123456789.987654321m literal (decimal return type).

        // TODO #1-8: Add the static method here with name "ReturnString" that should return "Hello, world!" literal (string return type).

        // TODO #1-9: Add the static method here with name "ReturnChar" that should return 'A' literal (char return type).

        // TODO #1-10: Add the static method here with name "ReturnByte" that should return 0xAB literal (byte return type).

        // TODO #1-11: Add the static method here with name "ReturnBool" that should return true literal (bool return type).
    }
}
